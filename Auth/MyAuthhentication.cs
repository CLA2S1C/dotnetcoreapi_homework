using System.Linq;
using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using onTraining.Database;
using Microsoft.AspNetCore.Mvc;

namespace onTraining.Auth
{
    public class MyAuthhentication : AuthorizeAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context){
            try
            {
                var dbContext = context.HttpContext.RequestServices.GetService(typeof(APIDBContext)) as APIDBContext; //TODO เป็นการ conntext database อีกวิธีหนึ่งในการเรียกใช้งานได้ง่ายขึ้น
                var config = context.HttpContext.RequestServices.GetService(typeof(IConfiguration)) as IConfiguration;
                var user = context.HttpContext.User;
                if(!user.Identity.IsAuthenticated) throw new Exception();
                string user_guid = user.Claims.First(c => c.Type == "user_guid").Value;
                if(dbContext.Users.FirstOrDefault(x => x.guid == new Guid(user_guid)) == null) throw new Exception();
            }
            catch (Exception)
            {
                context.Result = new UnauthorizedResult(); //! เป็นการเซ็ค ให้ Auth เป็น Unauthorized
                return;
            }
        }
    }
}