using System.Security.Claims;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;
using onTraining.Database;
using onTraining.Models;
using onTraining.Auth;
using System.Transactions;

namespace onTraining.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MyCoffeeStore : ControllerBase
    {
        public readonly APIDBContext _context;
        private readonly IConfiguration _config;
        public MyCoffeeStore(APIDBContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }
        #region -- Auth  RegisterNewEmplyee | LoginByEmplyee --
        [AllowAnonymous]
        [HttpPost("RegisterNewEmplyee")]
        public async Task<ActionResult<RespnseMessage>> RegisterNewEmplyee([FromBody]User user)
        {
          try
          {
            var chkUser = await _context.Users.FirstOrDefaultAsync(x => x.userName.Equals(user.userName));
            if(chkUser != null) return BadRequest(new RespnseMessage{statusCode = (int)System.Net.HttpStatusCode.BadRequest, Message = "Username ซ้ำ" });
            User _user = new User
            {
                guid = Guid.NewGuid(),
                userName = user.userName.Trim(),
                password = user.password.Trim(),
                firstName = user.firstName.Trim(),
                lastName = user.lastName.Trim(),
                email = user.email.Trim(),
                mobile = user.mobile.Trim(),
            };
            await _context.Users.AddAsync(_user);
            await _context.SaveChangesAsync();
            string token = GenerateJWTToken(_user.guid.ToString());
            return Ok(new RespnseMessage { statusCode = (int)System.Net.HttpStatusCode.OK, Message = token});
          }
          catch (Exception ex)
          {
              return StatusCode((int)System.Net.HttpStatusCode.InternalServerError, new RespnseMessage{statusCode = (int)System.Net.HttpStatusCode.InternalServerError, Message = ex.Message});
          }
        }
        [AllowAnonymous]
        [HttpGet("Login")]
        public async Task<IActionResult> Login([FromQuery] string userName, [FromQuery] string password)
        {
          try
          {
              //HttpContext.User.Identity.IsAuthenticated = true // Auth On
              //HttpContext.User.Claims.Where(p => p.Type == "user_guid").Value //! สำหรับค้นหาสิทธิ์
              //HttpContent.User.Claims.Where();
            if(string.IsNullOrEmpty(userName)) return BadRequest(new RespnseMessage{statusCode = (int)System.Net.HttpStatusCode.BadRequest, Message = "กรอก Username ด้วยน้าาา"});
            if(string.IsNullOrEmpty(password)) return BadRequest(new RespnseMessage{statusCode = (int)System.Net.HttpStatusCode.BadRequest, Message = "กรอก Password ด้วยน้าาา"});

            var _user = await _context.Users.FirstOrDefaultAsync(x => x.userName.Equals(userName) && x.password.Equals(password));
            if(_user == null) return BadRequest(new RespnseMessage{statusCode = (int)System.Net.HttpStatusCode.BadRequest, Message = "กรอก Username และ Password ไม่ถูกต้อง"});

            string token = GenerateJWTToken(_user.guid.ToString());
            return Ok(new RespnseMessage{statusCode = (int)System.Net.HttpStatusCode.OK, Message = token});
          }
          catch (Exception ex)
          {
              return StatusCode((int)System.Net.HttpStatusCode.InternalServerError, new RespnseMessage{statusCode = (int)System.Net.HttpStatusCode.InternalServerError, Message = ex.Message});
          }
        }
        #endregion
        #region Product SearchAllProduct | CreateProduct | UpdateProduct | DeleteProduct
        [HttpGet("SearchAllProduct")]
        public async Task<ActionResult<List<Product>>> SearchAllProduct()
        {
            try
            {
                var result = await _context.Products.Include(i => i.ProductDetails).Where(s => !s.isDeleted).ToListAsync();
                result.ForEach(f => f.ProductDetails.ForEach(s => s.Product = null));
                return result;
            }
            catch (System.Exception)
            {
                throw;
            }
        }
        [HttpPost("CreateProduct")]
        public async Task<ActionResult<Product>> CreateProduct([FromBody] ProductCreate product)
        {
            try
            {
              Product create = new Product{ProductName = product.ProductName.Trim(),ProductDetails = new List<ProductDetail>()};
              product.CreateProductDetails.ForEach(x => {
                create.ProductDetails.Add(new ProductDetail{
                  ProductDetailName = x.ProductDetailName.Trim(),
                  ProductDetailPrice = x.ProductDetailPrice
                });
              });
              await _context.Products.AddAsync(create);
              await _context.SaveChangesAsync();
              create.ProductDetails.ForEach(x => x.Product = null);
              return create;
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [HttpPost("UpdateProduct")]
        public async Task<IActionResult> UpdateProduct([FromBody]UpdateProduct product)
        {
            Product result = null;
            bool isBadRequest = false;
            try
            {
                if (product.ProductId == 0) throw new Exception("กรุณาระบุรหัสสินค้าที่ต้องการแก้ไข");
                result = await _context.Products.Include(x => x.ProductDetails).FirstOrDefaultAsync(s => s.ProductId == product.ProductId);
                if (result == null) throw new Exception($"ไม่พบรหัสสินค้า: {product.ProductId}");
                if (string.IsNullOrEmpty(product.ProductName)) throw new Exception("กรุณากรอกชื่อสินค้า");
                if(product.UpdateProductDetails != null){
                  foreach (var item in product.UpdateProductDetails)
                  {
                    var productDetail = await _context.ProductDetails.FirstOrDefaultAsync(f => f.ProductDetailtId == item.ProductDetailtId);
                    if(productDetail == null) throw new Exception($"สินค้าไม่มีรหัสประเภท {item.ProductDetailtId}");
                    foreach (var i in result.ProductDetails)
                    {
                        if(item.ProductDetailtId == i.ProductDetailtId){i.ProductDetailName = item.ProductDetailName.Trim(); i.ProductDetailPrice = item.ProductDetailPrice;};
                    }
                  }
                }
                result.ProductName = product.ProductName.Trim();
                _context.Update(result);
                await _context.SaveChangesAsync();
                result.ProductDetails.ForEach(x => x.Product = null);
            }
            catch (Exception ex)
            {
                isBadRequest = true;
                result = new Product { ProductName = ex.Message };
            }
            if (isBadRequest) return BadRequest(result);
            return Ok(result);
        }
        [HttpDelete("DeleteProduct/{id}")]
        public async Task<ActionResult<RespnseMessage>> Delete(int id)
        {
          try
          {
              var prod = await _context.Products.Where(x => x.ProductId == id).FirstOrDefaultAsync();
              if(prod == null) return BadRequest(new RespnseMessage{statusCode = (int)System.Net.HttpStatusCode.BadRequest, Message = $"ไม่พบรหัสสินค้า {id}"});
              prod.isDeleted = true;
              _context.Update(prod);
              await _context.SaveChangesAsync();
              return Ok(new RespnseMessage{statusCode = (int)System.Net.HttpStatusCode.OK, Message = $"ลบข้อมูลสินค้ารหัส {id} เรียบร้อย"});
          }
          catch (Exception ex)
          {
            return StatusCode((int)System.Net.HttpStatusCode.InternalServerError, new RespnseMessage{ statusCode = (int)System.Net.HttpStatusCode.InternalServerError, Message = ex.Message});
          }
        }
        #endregion
        #region  -- Transaction Create | Search | Delete --
        [MyAuthhentication]
        [HttpPost("CreateTransaction")]
        public async Task<ActionResult> CreateTransaction([FromBody]CreateTransaction CreateTransaction)
        {
          TransactionScope transactionRoll = new TransactionScope();
          string txtBad;
          try
          {
            List<TransactionDetail> AddTransactionsDetail = new List<TransactionDetail>();
            decimal _money = 0;
            var _users = await _context.Users.FirstOrDefaultAsync(x => x.guid == new Guid(HttpContext.User.Claims.FirstOrDefault(s => s.Type == "user_guid").Value));
            foreach (var item in CreateTransaction.TransactionDetails)
            {
              txtBad = item.ProductDetailId == 0 ? $"ลืมใส่รหัสสินค้ามาหรือป่าว {item.ProductDetailId}" : $"ลืมใส่จำนวนรหัสสินค้า {item.ProductDetailId}";
              if(item.ProductDetailId == 0 || item.Quantity == 0) throw new Exception(txtBad);
              var _search = await _context.ProductDetails.Where(s => s.ProductDetailtId == item.ProductDetailId).FirstOrDefaultAsync();
              txtBad = _search == null ? $"ไม่เจอรหัสสินค้า {item.ProductDetailId}" : "";
              if(_search == null) throw new Exception(txtBad);
              AddTransactionsDetail.Add(new TransactionDetail{
                ProductDetailId = _search.ProductDetailtId,
                Quantity = item.Quantity,
                TotalPrice = _search.ProductDetailPrice * item.Quantity
              });
              _money += _search.ProductDetailPrice * item.Quantity;
            }
              decimal totalPrice = CreateTransaction.ReceivedMoney - _money;
              txtBad = totalPrice < 0 ? $"ลูกค้าให้มาแค่ {CreateTransaction.ReceivedMoney} เองขาดอีก {totalPrice.ToString().Replace("-", "")} บาท" : "";
              if(totalPrice < 0)  throw new Exception(txtBad);
              var transactionLog = new Models.Transaction
              {
                UserId = _users.id,
                ReceivedMoney = CreateTransaction.ReceivedMoney,
                TotalTransactionDetailPrice = _money,
                ReturnMoney = totalPrice,
                TransactionDate = DateTime.Now,
                TransactionDetails = AddTransactionsDetail,
              };
               await _context.Transactions.AddAsync(transactionLog);
              await _context.SaveChangesAsync();
              transactionRoll.Complete();
            string result = $"รหัสใบเสร็จ :{transactionLog.TransactionId} | รหัสพนักงาน :{_users.id} | ชื่อพนักงาน :{_users.firstName}  {_users.lastName} | เงินที่ได้รับ :{transactionLog.TotalTransactionDetailPrice} | เงินทอน :{transactionLog.ReturnMoney} | วันเวลาทำรายการ :{transactionLog.TransactionDate}";
            //transactionLog.User.Transactions = null;
            //transactionLog.TransactionDetails.ForEach(x => x.Transaction = null);
            //return transactionLog;
            return StatusCode((int)System.Net.HttpStatusCode.OK, new RespnseMessage{statusCode = (int)System.Net.HttpStatusCode.OK, Message = result});
          }
          catch (Exception ex)
          {
            return StatusCode((int)System.Net.HttpStatusCode.InternalServerError, new RespnseMessage{statusCode = (int)System.Net.HttpStatusCode.InternalServerError, Message = ex.Message});
          }
        }
        [MyAuthhentication]
        [HttpGet("SearchTransactions/{id}")]
        public async Task<ActionResult<Models.Transaction>> SearchTransactions(int id)
        {
          try
          {
            var result = await _context.Transactions.Include(i => i.TransactionDetails).FirstOrDefaultAsync(f => f.TransactionId == id);
            result.User.Transactions = null;
            result.TransactionDetails.ForEach(x => { x.ProductDetail = null; x.Transaction = null;});
            return Ok(result);
          }
          catch (Exception ex)
          {
            return BadRequest(ex.Message);
          }
        }
         [HttpDelete("DeleteTransaction/{id}")]
         public async Task<ActionResult> DeleteTransaction(int id)
         {
           try
           {
             var transaction = await _context.Transactions.FirstOrDefaultAsync(f => f.TransactionId == id);
             if(transaction == null) throw new Exception($"ไม่พบเลขใบเสร็จที่ {id}");
             var transactionDetail = await _context.TransactionDetails.Where(w => w.TransactionId == id).ToListAsync();
             _context.TransactionDetails.RemoveRange(transactionDetail);
             _context.Transactions.Remove(transaction);
             await _context.SaveChangesAsync();
             return StatusCode(200, new RespnseMessage{statusCode = 200, Message = $"ใบเสร็จถูกลบเรียบร้อย {id}"});
           }
           catch (Exception ex)
           {
             return BadRequest(ex.Message);
           }
         }
        #endregion
        #region JWT
        private string GenerateJWTToken(string userGuid){
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddHours(1);

            var token = new JwtSecurityToken(
                issuer: _config["JwtIssuer"],
                audience: _config["JwtAudience"],
                claims: new List<Claim>{new Claim("user_guid", userGuid)},
                expires: expires,
                signingCredentials: creds
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        #endregion
    }
}