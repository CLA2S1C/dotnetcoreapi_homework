using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using onTraining.Database;
using onTraining.Models;

namespace onTraining.Services
{
    public interface IDbInitializer
    {
         Task DataSeeding();
    }
    public class DBInitializer : IDbInitializer
    {
        private readonly APIDBContext _context;
        public DBInitializer(APIDBContext context)
        {
            _context = context;
        }
        public async Task DataSeeding()
        {
          try
          {
              await _context.AddRangeAsync(new List<Product>{
                  new Product {ProductName = "กาแฟ", ProductDetails = new List<ProductDetail>
                  {
                      new ProductDetail{ ProductDetailName = "เย็น", ProductDetailPrice = 45},
                      new ProductDetail{ ProductDetailName = "ร้อน", ProductDetailPrice = 30},
                  }},
                  new Product {ProductName = "อเมซอน", ProductDetails = new List<ProductDetail>
                  {
                      new ProductDetail{ ProductDetailName = "เย็น", ProductDetailPrice = 50},
                      new ProductDetail{ ProductDetailName = "ร้อน", ProductDetailPrice = 40}
                  }},
                  new Product {ProductName = "คาปูชิโน่", ProductDetails = new List<ProductDetail>
                  {
                      new ProductDetail{ ProductDetailName = "เย็น", ProductDetailPrice = 50},
                      new ProductDetail{ ProductDetailName = "ร้อน", ProductDetailPrice = 40}
                  }},
              });
              await _context.Users.AddAsync(new User
              {
                  userName = "admin",
                  password = "1234",
                  firstName = "Suchuj",
                  lastName = "Kaenasa",
                  guid = Guid.NewGuid(),
                  email = "suchujkaenasa@gmail.com",
                  mobile = "0987654321",
              });
              await _context.SaveChangesAsync();
          }
          catch {}
        }
    }
}