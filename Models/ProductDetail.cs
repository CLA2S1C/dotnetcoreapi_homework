using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace onTraining.Models
{
    public class ProductDetail
    {
        [Key]
        public int ProductDetailtId { get; set; }
        public int ProductId { get; set; }
        [Required]
        public string ProductDetailName { get; set; }
        [Required]
        public decimal ProductDetailPrice { get; set; }
        [ForeignKey(nameof(ProductId))]
        public Product Product { get; set; }
    }
    public class UpdateProductDetail
    {
        [Key]
        public int ProductDetailtId { get; set; }
        [Required]
        public string ProductDetailName { get; set; }
        [Required]
        public decimal ProductDetailPrice { get; set; }
    }
    public class CreateProductDetail
    {
        [Required]
        public string ProductDetailName { get; set; }
        [Required]
        public decimal ProductDetailPrice { get; set; }
    }
}