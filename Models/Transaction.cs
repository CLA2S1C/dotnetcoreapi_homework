using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace onTraining.Models
{
    public class Transaction
    {
        [Key]
        public int TransactionId { get; set; }
        public int UserId { get; set; }
        public decimal ReceivedMoney { get; set; }
        public decimal TotalTransactionDetailPrice { get; set; }
        public decimal ReturnMoney { get; set; }
        [Required]
        public DateTime TransactionDate { get; set; }
        [ForeignKey(nameof(UserId))]
        public User User { get; set; }
        public List<TransactionDetail> TransactionDetails { get; set; }
    }
    public class CreateTransaction
    {
        public decimal ReceivedMoney { get; set; }
        public List<CreateTransactionDetail> TransactionDetails { get; set; }
    }
}