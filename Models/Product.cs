using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Transactions;
namespace onTraining.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        [Required]
        public string ProductName { get; set; }
        public bool isDeleted { get; set; } = false;
        public List<ProductDetail> ProductDetails { get; set; }
    }
    public class ProductCreate
    {
        [Required]
        public string ProductName { get; set; }
        public List<CreateProductDetail> CreateProductDetails { get; set; }
    }
    public class UpdateProduct
    {
        [Required]
        public int ProductId { get; set; }
        [Required]
        public string ProductName { get; set; }
        public List<UpdateProductDetail> UpdateProductDetails { get; set; }
    }
}