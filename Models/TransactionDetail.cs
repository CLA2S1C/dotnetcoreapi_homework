using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace onTraining.Models
{
    public class TransactionDetail
    {
        [Key]
        public int TransactionDetailld { get; set; }
        [Required]
        public int TransactionId { get; set; }
        [Required]
        public int ProductDetailId { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public decimal TotalPrice { get; set; }
        [ForeignKey(nameof(ProductDetailId))]
        public ProductDetail ProductDetail { get; set; }
        [ForeignKey(nameof(TransactionId))]
        public Transaction Transaction { get; set; }
    }
    public class CreateTransactionDetail
    {
        [Required]
        public int ProductDetailId { get; set; }
        [Required]
        public int Quantity { get; set; }
    }
}