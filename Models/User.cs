using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace onTraining.Models
{
    public class User
    {
        [Key]
        public int id { get; set; }
        [Required]
        public Guid guid { get; set; }
        [Required]
        public string userName { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public string firstName { get; set; }
        [Required]
        public string lastName { get; set; }
        public string email { get; set; }
        [Required]
        public string mobile { get; set; }
        public List<Transaction> Transactions { get; set; }
    }
}