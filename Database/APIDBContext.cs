using Microsoft.EntityFrameworkCore;
using onTraining.Models;

namespace onTraining.Database
{
    public class APIDBContext : DbContext
    {
        public APIDBContext (DbContextOptions<APIDBContext> options) : base(options){}
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductDetail> ProductDetails { get; set; }
        public DbSet<TransactionDetail> TransactionDetails { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<User> Users { get; set; }
        // protected override void OnModelCreating(ModelBuilder modelBuilder){
        //     base.OnModelCreating(modelBuilder);
        //     modelBuilder.Entity<Transaction>().HasMany(p => p.TransactionDetails).WithOne(w => w.Transaction).OnDelete(DeleteBehavior.Cascade);
        // }
    }
}